﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompSpot : MonoBehaviour
{
    public GameObject Player;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("hi");
        if (other.tag == "Player")
        {
            //Destroy the enemy
            Debug.Log("on head");
            Rigidbody playerRB = Player.GetComponent<Rigidbody>();
            playerRB.velocity = new Vector2(0, 0);
            playerRB.AddForce(Vector2.up * 10, ForceMode.Impulse);

            Destroy(transform.parent.gameObject);
        }
    }
}
