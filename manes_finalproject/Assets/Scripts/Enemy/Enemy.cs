﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private string currentState = "IdleState";
    private Transform target;
    public float chaseRange = 3;
    public float attackRange = 2;
    public float speed = 3;

    public int health;
    public int maxHealth;
    public int attack;


    //public Animator animator;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, target.position);

        if(currentState == "IdleState")
        {
            if (distance < chaseRange)
                currentState = "ChaseState";
        }
        else if(currentState == "ChaseState")
        {
            //animator.SetTrigger("chase");
            //animator.SetBool("isAttacking", false);

            if (distance < attackRange)
            {
                currentState = "AttackState";
                InvokeRepeating("Attack", 1f, 1f);
                Debug.Log("begin Attack");
            }

            if(target.position.x > transform.position.x)
            {
                //move right
                transform.Translate(transform.right * speed * Time.deltaTime);
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                //move left
                transform.Translate(-transform.right * speed * Time.deltaTime);
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }

        }
        else if(currentState == "AttackState")
        {
            //animator.SetBool("isAttacking", true);
            if (distance > attackRange)
            {
                currentState = "ChaseState";
                CancelInvoke("Attack");
                Debug.Log("Stopped Invoke");
            }
        }
    }

    public void Attack()
    {
        PlayerManager.currentHealth -= attack;
        Debug.Log("Attack happening");
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Projectile"))
        {
            TakeDamage(3);
            Debug.Log("Taken Damage");
        }
    }
    */
    public void TakeDamage(int damage)
    {
        health -= damage;


        if(health < 0)
        {
            Die();
        }
    }

    private void Die()
    {
        // play death animation
        //animator.SetTrigger("isDead");
        // disable the script and the enemy collider
        Destroy(gameObject);
    }
}
