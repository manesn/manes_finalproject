﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apocalypse : MonoBehaviour
{
    public Transform sun; //A dragged in reference to the Sun's transform

    public Vector3 sunStart;

    public Vector3 sunEnd;

    public float timerStartValue; //How much time the timer starts with at the beginning of the game (so we know how long has passed)

    void Update()
    {

        //*LerpAmount: A number ranging from 0f-1f, with 0 representing the timer being completely exhausting and 1f representing the timer being at timerStartValue, .5f would me the timer is half finished*/

        float lerpAmount = PlayerManager.timeLeft / timerStartValue;

        sun.position = Vector3.Lerp(sunEnd, sunStart, lerpAmount); //Blend between the sun being up or down based on lerpAmount

    }
}
