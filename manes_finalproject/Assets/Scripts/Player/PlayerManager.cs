﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    public static int numberOfCoins;
    public TextMeshProUGUI numberOfCoinsText;
    public TextMeshProUGUI countDown;

    public static int currentHealth = 100;
    public Slider healthBar;
 
    public static bool gameOver;

    public static float timeLeft = 60f;

    public GameObject gameOverPanel;
    public GameObject winPanel;
    public GameObject Player;
    public GameObject Sun;

    void Start()
    {
        numberOfCoins = 0;
        gameOver = false;
    }

    
    void Update()
    {

        if (PlayerManager.gameOver)
        {
            Destroy(gameObject);
        }

        numberOfCoinsText.text = "shards: " + numberOfCoins;

        // health
        healthBar.value = currentHealth;

        // game over, oh no
        if (currentHealth < 0)
        {
            gameOver = true;
            gameOverPanel.SetActive(true);
            currentHealth = 100;
            Player.SetActive(false);
        }

        if(numberOfCoins >= 50)
        {
            winPanel.SetActive(true);
            timeLeft = 10000f;
            Sun.SetActive(false);
        }

        timeLeft -= Time.deltaTime;
        // print("Time Left Until The End: " + timeLeft);
        countDown.text = " " + timeLeft;
        if (timeLeft <= 0f)
        {
            gameOver = true;
            gameOverPanel.SetActive(true);
            currentHealth = 100;
            Player.SetActive(false);
        }

    }
}
