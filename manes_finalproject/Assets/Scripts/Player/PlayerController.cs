﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController controller;
    private Vector3 direction;
    public float speed = 8;

    public float jumpForce = 10;
    public float gravity = -20;
    public Transform groundCheck;
    public LayerMask groundLayer;

    public bool ableToMakeADoubleJump = true;

    PlayerAttacks playerAttacks;
    public Transform model;
    public GameObject fireBallPivot;

    void Start()
    {
        playerAttacks = GetComponent<PlayerAttacks>();
    }

    void Update()
    {
        float hInput = Input.GetAxis("Horizontal"); // left right
        direction.x = hInput * speed;

        if (direction.x > 0)
        {
            model.transform.localScale = new Vector3(2.644244f, model.transform.localScale.y, model.transform.localScale.z);
            fireBallPivot.transform.localScale = new Vector3(1, 1, 1);
        }
        else if (direction.x < 0)
        {
            model.transform.localScale = new Vector3(-2.644244f, model.transform.localScale.y, model.transform.localScale.z);
            fireBallPivot.transform.localScale = new Vector3(-1, 1, 1); // Flips the fireball point
        }


        bool isGrounded = Physics.CheckSphere(groundCheck.position, 0.15f, groundLayer);
        direction.y += gravity * Time.deltaTime;

        if (isGrounded)
        {
            direction.y = -1;
            ableToMakeADoubleJump = true;
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }

            if(Input.GetKeyDown(KeyCode.F))    
            {
                playerAttacks.FireBallAttack();
            }
        }
        else
        {
            direction.y += gravity * Time.deltaTime;
            if (ableToMakeADoubleJump & Input.GetButtonDown("Jump"))
            {
                DoubleJump();
            }
        }
        if (hInput != 0)
        {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, 0));
            model.rotation = newRotation;
        }

        controller.Move(direction * Time.deltaTime);

        if (transform.position.z != 0)
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);

    }

        private void DoubleJump()
        {
            direction.y = jumpForce;
            ableToMakeADoubleJump = false;
        }

        private void Jump()
        {
            direction.y = jumpForce;
        }
  
}
